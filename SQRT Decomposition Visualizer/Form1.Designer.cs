﻿namespace SQRT_Decomposition_Visualizer
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.source_array_panel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.support_array_panel = new System.Windows.Forms.Panel();
            this.arrayCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.setArraySize = new System.Windows.Forms.Button();
            this.stepCounter = new System.Windows.Forms.Label();
            this.nextStep = new System.Windows.Forms.Button();
            this.prevStep = new System.Windows.Forms.Button();
            this.stepDescription = new System.Windows.Forms.Label();
            this.stepsPanel = new System.Windows.Forms.Panel();
            this.autoPlay = new System.Windows.Forms.Button();
            this.varDump = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.queryL = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.queryR = new System.Windows.Forms.TextBox();
            this.setQuery = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.stepsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // source_array_panel
            // 
            this.source_array_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.source_array_panel.AutoScroll = true;
            this.source_array_panel.Location = new System.Drawing.Point(12, 121);
            this.source_array_panel.Name = "source_array_panel";
            this.source_array_panel.Size = new System.Drawing.Size(1236, 104);
            this.source_array_panel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(8, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "ИСХОДНЫЙ МАССИВ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(8, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(318, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "ВСПОМОГАТЕЛЬНЫЙ МАССИВ";
            // 
            // support_array_panel
            // 
            this.support_array_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.support_array_panel.AutoScroll = true;
            this.support_array_panel.Location = new System.Drawing.Point(12, 260);
            this.support_array_panel.Name = "support_array_panel";
            this.support_array_panel.Size = new System.Drawing.Size(1236, 98);
            this.support_array_panel.TabIndex = 4;
            // 
            // arrayCount
            // 
            this.arrayCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrayCount.Location = new System.Drawing.Point(12, 43);
            this.arrayCount.MaxLength = 2;
            this.arrayCount.Name = "arrayCount";
            this.arrayCount.Size = new System.Drawing.Size(51, 29);
            this.arrayCount.TabIndex = 7;
            this.arrayCount.Text = "25";
            this.arrayCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(8, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(214, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Изменить размер массива:";
            // 
            // setArraySize
            // 
            this.setArraySize.Location = new System.Drawing.Point(69, 43);
            this.setArraySize.Name = "setArraySize";
            this.setArraySize.Size = new System.Drawing.Size(76, 29);
            this.setArraySize.TabIndex = 9;
            this.setArraySize.Text = "СОЗДАТЬ";
            this.setArraySize.UseVisualStyleBackColor = true;
            this.setArraySize.Click += new System.EventHandler(this.setArraySize_Click);
            // 
            // stepCounter
            // 
            this.stepCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stepCounter.Location = new System.Drawing.Point(31, 47);
            this.stepCounter.Name = "stepCounter";
            this.stepCounter.Size = new System.Drawing.Size(139, 23);
            this.stepCounter.TabIndex = 10;
            this.stepCounter.Text = "0/0";
            this.stepCounter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nextStep
            // 
            this.nextStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextStep.Location = new System.Drawing.Point(166, 42);
            this.nextStep.Name = "nextStep";
            this.nextStep.Size = new System.Drawing.Size(32, 33);
            this.nextStep.TabIndex = 11;
            this.nextStep.Text = ">";
            this.nextStep.UseVisualStyleBackColor = true;
            this.nextStep.Click += new System.EventHandler(this.nextStep_Click);
            // 
            // prevStep
            // 
            this.prevStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prevStep.Location = new System.Drawing.Point(0, 41);
            this.prevStep.Name = "prevStep";
            this.prevStep.Size = new System.Drawing.Size(32, 33);
            this.prevStep.TabIndex = 12;
            this.prevStep.Text = "<";
            this.prevStep.UseVisualStyleBackColor = true;
            this.prevStep.Click += new System.EventHandler(this.prevStep_Click);
            // 
            // stepDescription
            // 
            this.stepDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stepDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stepDescription.Location = new System.Drawing.Point(-3, 149);
            this.stepDescription.Margin = new System.Windows.Forms.Padding(0);
            this.stepDescription.Name = "stepDescription";
            this.stepDescription.Size = new System.Drawing.Size(1236, 87);
            this.stepDescription.TabIndex = 13;
            this.stepDescription.Text = "stepDescription";
            this.stepDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stepsPanel
            // 
            this.stepsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stepsPanel.Controls.Add(this.autoPlay);
            this.stepsPanel.Controls.Add(this.varDump);
            this.stepsPanel.Controls.Add(this.label9);
            this.stepsPanel.Controls.Add(this.nextStep);
            this.stepsPanel.Controls.Add(this.stepCounter);
            this.stepsPanel.Controls.Add(this.prevStep);
            this.stepsPanel.Controls.Add(this.stepDescription);
            this.stepsPanel.Location = new System.Drawing.Point(12, 364);
            this.stepsPanel.Name = "stepsPanel";
            this.stepsPanel.Size = new System.Drawing.Size(1236, 285);
            this.stepsPanel.TabIndex = 14;
            // 
            // autoPlay
            // 
            this.autoPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.autoPlay.Location = new System.Drawing.Point(0, 80);
            this.autoPlay.Name = "autoPlay";
            this.autoPlay.Size = new System.Drawing.Size(198, 33);
            this.autoPlay.TabIndex = 20;
            this.autoPlay.Text = "АВТОВЫПОЛНЕНИЕ";
            this.autoPlay.UseVisualStyleBackColor = true;
            this.autoPlay.Click += new System.EventHandler(this.autoPlay_Click);
            // 
            // varDump
            // 
            this.varDump.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.varDump.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.varDump.Location = new System.Drawing.Point(0, 236);
            this.varDump.Margin = new System.Windows.Forms.Padding(0);
            this.varDump.Name = "varDump";
            this.varDump.Size = new System.Drawing.Size(1236, 49);
            this.varDump.TabIndex = 19;
            this.varDump.Text = "varDump";
            this.varDump.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(-4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(288, 24);
            this.label9.TabIndex = 18;
            this.label9.Text = "ПОШАГОВОЕ ВЫПОЛНЕНИЕ";
            // 
            // queryL
            // 
            this.queryL.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.queryL.Location = new System.Drawing.Point(320, 43);
            this.queryL.MaxLength = 3;
            this.queryL.Name = "queryL";
            this.queryL.Size = new System.Drawing.Size(51, 29);
            this.queryL.TabIndex = 15;
            this.queryL.Text = "5";
            this.queryL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(290, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(219, 20);
            this.label6.TabIndex = 16;
            this.label6.Text = "Запрос суммы в диапазоне:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(291, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 16);
            this.label7.TabIndex = 17;
            this.label7.Text = "от";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(395, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 16);
            this.label8.TabIndex = 18;
            this.label8.Text = "до";
            // 
            // queryR
            // 
            this.queryR.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.queryR.Location = new System.Drawing.Point(425, 43);
            this.queryR.MaxLength = 3;
            this.queryR.Name = "queryR";
            this.queryR.Size = new System.Drawing.Size(51, 29);
            this.queryR.TabIndex = 19;
            this.queryR.Text = "10";
            this.queryR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // setQuery
            // 
            this.setQuery.Location = new System.Drawing.Point(482, 43);
            this.setQuery.Name = "setQuery";
            this.setQuery.Size = new System.Drawing.Size(88, 29);
            this.setQuery.TabIndex = 20;
            this.setQuery.Text = "ИЗМЕНИТЬ";
            this.setQuery.UseVisualStyleBackColor = true;
            this.setQuery.Click += new System.EventHandler(this.setQuery_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 4000;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 661);
            this.Controls.Add(this.setQuery);
            this.Controls.Add(this.queryR);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.queryL);
            this.Controls.Add(this.stepsPanel);
            this.Controls.Add(this.setArraySize);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.arrayCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.support_array_panel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.source_array_panel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Визуализатор SQRT-декомпозиции";
            this.stepsPanel.ResumeLayout(false);
            this.stepsPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel source_array_panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel support_array_panel;
        private System.Windows.Forms.TextBox arrayCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button setArraySize;
        private System.Windows.Forms.Label stepCounter;
        private System.Windows.Forms.Button nextStep;
        private System.Windows.Forms.Button prevStep;
        private System.Windows.Forms.Label stepDescription;
        private System.Windows.Forms.Panel stepsPanel;
        private System.Windows.Forms.TextBox queryL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox queryR;
        private System.Windows.Forms.Button setQuery;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label varDump;
        private System.Windows.Forms.Button autoPlay;
        private System.Windows.Forms.Timer timer1;
    }
}

