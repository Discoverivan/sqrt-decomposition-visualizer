﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQRT_Decomposition_Visualizer
{
    public partial class Form1 : Form
    {
        SQRTDecompositionSum sqrtdsum;
        int qL, qR, arraySize;
        Steps steps;
        bool autoPlayState = false;

        class SQRTDecompositionSum
        {
            List<int> source, support;
            int segment_length;
            Steps steps;
            public SQRTDecompositionSum(List<int> src, ref Steps steps)
            {
                source = src;
                this.steps = steps;
                CalculateSupportArray();
            }
            public int GetSegmentLength()
            {
                return segment_length;
            }
            public List<int> GetSupportArray()
            {
                return support;
            }
            void CalculateSupportArray()
            {
                segment_length = (int)Math.Sqrt(source.Count) + 1;

                Step s = new Step();
                s.description = "Вычислен размер сегмента. Размер сегмента = " + segment_length;
                s.segment_size = segment_length;
                s.segments_calculated = -1;
                steps.AddStep(s);

                support = new List<int>(new int[(source.Count / segment_length) + 1]);

                List<int> supportStepHelp = new List<int>();
                for (int i = 0; i < (source.Count / segment_length) + 1; i++)
                {
                    supportStepHelp.Add(0); 
                }

                s = new Step();
                s.description = "Инициалирован вспомогательный массив.";
                s.support_state = new List<int>();
                s.segments_calculated = -1;
                s.support_state.AddRange(supportStepHelp);
                steps.AddStep(s);

                for (int i = 0; i < source.Count; ++i)
                    support[i / segment_length] += source[i];

                for (int i=0; i < (source.Count/segment_length)+1; i++)
                {
                    supportStepHelp[i] = support[i];
                    s = new Step();
                    s.description = "Вычислена сумма сегмента " + i + ".\r\nДля этого произведено суммирование выделенных элементов в исходном массиве.";
                    s.support_state = new List<int>();
                    s.segments_calculated = -1;
                    s.support_state.AddRange(supportStepHelp);
                    s.highlight_source = new List<int>();
                    s.highlight_support = new List<int>();
                    s.highlight_support.Add(i);
                    for (int j = segment_length * i; j < segment_length * (i + 1) && j<source.Count; j++)
                    {
                        s.highlight_source.Add(j);
                    }
                    s.highlight_support.Add(i);
                    steps.AddStep(s);
                }

                s = new Step();
                s.description = "Завершен просчет вспомогательного массива.";
                s.support_state = new List<int>();
                s.segments_calculated = 1;
                s.support_state.AddRange(support);
                steps.AddStep(s);
            }

            public int GetRangeSum(int l, int r)
            {
                Step s = new Step();
                s.description = "Запрос суммы в диапазоне от " + l + " до " + r + ".";
                s.support_state = new List<int>();
                s.support_state.AddRange(support);
                s.highlight_source = new List<int>();
                for (int i = l; i <= r; i++)
                {
                    s.highlight_source.Add(i);
                }
                steps.AddStep(s);

                int sum = 0;
                int c_l = l / segment_length, c_r = r / segment_length;

                s = new Step();
                s.description = "Вычислены номера крайних сегментов: левый - " + c_l + ", правый - " + c_r + ".";
                s.support_state = new List<int>();
                s.support_state.AddRange(support);
                steps.AddStep(s);

                if (c_l == c_r)
                {
                    for (int i = l; i <= r; ++i)
                        sum += source[i];

                    s = new Step();
                    s.description = "Особый случай. Запрос находится в одном сегменте.Просто суммируем элементы в диапазоне.";
                    s.support_state = new List<int>();
                    s.support_state.AddRange(support);
                    s.highlight_source = new List<int>();
                    s.sum = sum;
                    for (int i = l; i <= r; i++)
                    {
                        s.highlight_source.Add(i);
                    }
                    steps.AddStep(s);
                }
                else
                {

                    s = new Step();
                    s.description = "Вычисление левого \"хвоста\".\r\nК общей сумме добавляются выделенные элементы.";
                    s.support_state = new List<int>();
                    s.support_state.AddRange(support);
                    s.highlight_source = new List<int>();
                    

                    for (int i = l, end = (c_l + 1) * segment_length - 1; i <= end; ++i)
                    {
                        sum += source[i];
                        s.highlight_source.Add(i);
                    }
                    s.sum = sum;
                    steps.AddStep(s);

                    s = new Step();
                    s.description = "Вычисление основной части.\r\nК общей сумме добавляются сегменты, которые полностью попали в запрос.";
                    s.support_state = new List<int>();
                    s.support_state.AddRange(support);
                    s.highlight_support = new List<int>();

                    int c = 0;
                    for (int i = c_l + 1; i <= c_r - 1; ++i)
                    {
                        sum += support[i];
                        s.highlight_support.Add(i);
                        c++;
                    }
                    s.sum = sum;
                    if (c > 0)
                        steps.AddStep(s);

                    s = new Step();
                    s.description = "Вычисление правого \"хвоста\".\r\nК общей сумме добавляются выделенные элементы.";
                    s.support_state = new List<int>();
                    s.support_state.AddRange(support);
                    s.highlight_source = new List<int>();

                    for (int i = c_r * segment_length; i <= r; ++i)
                    {
                        sum += source[i];
                        s.highlight_source.Add(i);
                    }
                    s.sum = sum;
                    steps.AddStep(s);
                }

                s = new Step();
                s.description = "Сумма в диапазоне от " + l + " до " + r + " равна " + sum + ".";
                s.support_state = new List<int>();
                s.support_state.AddRange(support);
                s.highlight_source = new List<int>();
                for (int i = l; i <= r; i++)
                {
                    s.highlight_source.Add(i);
                }
                steps.AddStep(s);

                return sum;
            }
        }

        struct Step
        {
            public string description;
            public List<int> highlight_source, highlight_support;
            public List<int> support_state;
            public int sum, segment_size, segments_calculated;
        }

        class Steps
        {
            View source, support;
            Panel description;
            List<Step> steps;
            int currentStep;
            int sum, segment_size, highlight_segments;

            public Steps(View source, View support, Panel description)
            {
                this.source = source;
                this.support = support;
                this.description = description;
                steps = new List<Step>();
            }

            public void AddStep(Step step)
            {
                this.steps.Add(step);
            }

            public void Load()
            {
                Label counter = description.Controls["stepCounter"] as Label;
                counter.Text = "1/" + steps.Count;
                currentStep = 1;
                LoadStep(1);
            }

            public void NextStep()
            {
                if (currentStep < steps.Count)
                {
                    currentStep++;
                    LoadStep(currentStep);
                }
            }

            public void PrevStep()
            {
                if (currentStep > 1)
                {
                    currentStep--;
                    LoadStep(currentStep);
                }
            }

            void LoadStep(int n)
            {
                (description.Controls["stepCounter"] as Label).Text = n + "/" + steps.Count;
                (description.Controls["stepDescription"] as Label).Text = steps[n - 1].description;

                if (steps[n-1].segments_calculated == -1 || steps[n - 1].segments_calculated == 1)
                {
                    highlight_segments = steps[n - 1].segments_calculated;
                }

                if (steps[n-1].segment_size != 0)
                {
                    segment_size = steps[n - 1].segment_size;
                }
                if (steps[n - 1].sum != 0)
                {
                    sum = steps[n - 1].sum;
                }
                (description.Controls["varDump"] as Label).Text = "Размер сегмента: " + segment_size + "\r\nОбщая сумма: " + sum;

                if (steps[n - 1].highlight_source != null)
                {
                    source.UnhighlightElements();
                    foreach (int e in steps[n - 1].highlight_source)
                    {
                        source.HighlightElement(e);
                    }
                } else
                {
                    source.UnhighlightElements();
                }

                if (steps[n - 1].support_state != null)
                {
                    support.DisplayArray(steps[n - 1].support_state);
                } else
                {
                    support.Clear();
                }

                if (steps[n - 1].highlight_support != null)
                {
                    support.UnhighlightElements();
                    foreach (int e in steps[n - 1].highlight_support)
                    {
                        support.HighlightElement(e);
                    }
                }
                else
                {
                    support.UnhighlightElements();
                }

                if (highlight_segments == 1)
                {
                    support.HighlightSegments(1);
                    source.HighlightSegments(segment_size);
                }
            }

            public int GetCurrentStep()
            {
                return currentStep;
            } 

            public int GetStepsCount()
            {
                return steps.Count;
            }
        }

        class View
        {
            Panel panel;
            string tag;
            int count;
            List<int> array;
            List<Color> colors = new List<Color>()
            {
                Color.FromArgb(219, 232, 255),
                Color.FromArgb(222, 255, 219),
                Color.FromArgb(255, 252, 219),
                Color.FromArgb(255, 225, 219),
                Color.FromArgb(249, 219, 255),
                Color.FromArgb(229, 229, 229)
            };

            public View(Panel panel, string tag)
            {
                this.panel = panel;
                this.tag = tag;
            }
            
            public void Clear()
            {
                panel.Controls.Clear();
            }

            public void DisplayArray(List<int> a)
            {
                array = a;
                panel.Visible = false;
                Clear();
                int x = 0;
                count = a.Count;
                for (int i = 0; i < a.Count; i++)
                {
                    Label tmp = new Label();
                    tmp.Text = a[i].ToString();
                    tmp.Font = new Font(tmp.Font.FontFamily, 14);
                    tmp.BorderStyle = BorderStyle.FixedSingle;
                    tmp.BackColor = Color.White;
                    tmp.AutoSize = true;
                    tmp.Padding = new Padding(10, 10, 10, 10);
                    tmp.Name = "array_" + tag + i;
                    tmp.Location = new Point(x, 20);
                    panel.Controls.Add(tmp);


                    Label tmp1 = new Label();
                    tmp1.Text = i.ToString();
                    tmp1.Font = new Font(tmp.Font.FontFamily, 10);
                    tmp1.ForeColor = Color.Gray;
                    tmp1.AutoSize = true;
                    tmp1.Name = "arrayN_" + tag + i;
                    tmp1.Location = new Point(x, 0);
                    panel.Controls.Add(tmp1);

                    x += (int)tmp.CreateGraphics().MeasureString(tmp.Text, tmp.Font).Width + 22;
                }
                panel.Visible = true;
            }

            public void HighlightElement(int n, Color? color = null)
            {
                Label e = panel.Controls["array_" + tag + n] as Label;
                e.BackColor = color ?? Color.Yellow;
            }

            public void HighlightSegments(int segment_size)
            {
                int clr_tmp = 0, c = 0;
                for(int i = 0; i < array.Count; i++)
                {
                    c++;
                    Label e = panel.Controls["array_" + tag + i] as Label;
                    if (e.BackColor != Color.Yellow)
                    {
                        e.BackColor = colors[clr_tmp];
                    }
                    if (c == segment_size)
                    {
                        clr_tmp++;
                        c = 0;
                    }
                    if (clr_tmp >= colors.Count) { clr_tmp = 0; }
                }
            }

            public void UnhighlightElements()
            {
                for (int i = 0; i < count; i++)
                {
                    Label e = panel.Controls["array_" + tag + i] as Label;
                    if (e == null)
                        continue;
                    e.BackColor = Color.White;
                }
            }
        }

        List<int> GenerateArray(int lenght, int min=0, int max = 100)
        {
            List<int> gen = new List<int>();
            Random r = new Random();
            for (int i = 0; i < lenght; i++)
            {
                gen.Add(r.Next(min,max));
            }
            return gen;
        }

        public Form1()
        {
            InitializeComponent();
            Random r = new Random();
            int rndSize = r.Next(5,30);
            arraySize = rndSize;
            arrayCount.Text = "" + rndSize;
            int rndL = (int)r.Next(0, rndSize/2);
            int rndR = (int)r.Next(rndL,rndSize);
            queryL.Text = "" + rndL;
            queryR.Text = "" + rndR;
            qL = rndL; qR = rndR;
            calculateSteps(Int32.Parse(arrayCount.Text));
            timer1.Tick += new EventHandler(autoPlayHandler);
        }

        void calculateSteps(int length, int min=0, int max= 100)
        {
            arraySize = length;
            List<int> source_array = GenerateArray(length, min, max);
            View source_array_view = new View(source_array_panel, "source");
            source_array_view.DisplayArray(source_array);
            View support_array_view = new View(support_array_panel, "support");
            steps = new Steps(source_array_view, support_array_view, stepsPanel);
            Step s = new Step();
            s.description = "Сгенерирован исходный массив, состоящий из " + length +" элементов.";
            steps.AddStep(s);
            sqrtdsum = new SQRTDecompositionSum(source_array, ref steps);
            sqrtdsum.GetRangeSum(qL,qR);
            steps.Load();
            StepButtonsControl();
            StepButtonsControl();
    }

        void StepButtonsControl()
        {
            if (autoPlayState)
            {
                prevStep.Enabled = false;
                nextStep.Enabled = false;
                return;
            }
            if (steps.GetCurrentStep() > 1)
            {
                prevStep.Enabled = true;
            }
            else
            {
                prevStep.Enabled = false;
            }
            if (steps.GetCurrentStep() < steps.GetStepsCount())
            {
                nextStep.Enabled = true;
            }
            else
            {
                nextStep.Enabled = false;
            }
        }

        void autoPlayHandler(object Sender, EventArgs e)
        {
            steps.NextStep();
            StepButtonsControl();
            if (steps.GetCurrentStep() >= steps.GetStepsCount())
            {
                autoPlayState = false;
                autoPlay.Text = "АВТОВЫПОЛНЕНИЕ";
                timer1.Enabled = false;
                StepButtonsControl();
            }
        }

        private void prevStep_Click(object sender, EventArgs e)
        {
            steps.PrevStep();
            StepButtonsControl();
        }

        private void nextStep_Click(object sender, EventArgs e)
        {
            steps.NextStep();
            StepButtonsControl();
        }

        private void autoPlay_Click(object sender, EventArgs e)
        {
            if (autoPlayState == false)
            {
                autoPlayState = true;
                autoPlay.Text = "ОСТАНОВИТЬ";
                timer1.Enabled = true;
                StepButtonsControl();
            } else
            {
                autoPlayState = false;
                autoPlay.Text = "АВТОВЫПОЛНЕНИЕ";
                timer1.Enabled = false;
            }
        }

        private void setArraySize_Click(object sender, EventArgs e)
        {
            int j;
            if (Int32.TryParse(arrayCount.Text, out j))
            {
                if (j > 0 && j<100)
                {
                    calculateSteps(j);
                } else
                {
                    MessageBox.Show("Введенное число должно быть больше 0 и меншье 100", "Ошибка");
                }
            }
            else
            {
                MessageBox.Show("Некорректный ввод","Ошибка");
            }
        }

        private void setQuery_Click(object sender, EventArgs e)
        {
            int l;
            if (Int32.TryParse(queryL.Text, out l))
            {
                if (l >= 0 && l < arraySize)
                {
                    qL = l;
                }
                else
                {
                    MessageBox.Show("Левая граница должна быть больше 0 и меншье "+ arraySize, "Ошибка");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Некорректный ввод", "Ошибка");
                return;
            }

            int r;
            if (Int32.TryParse(queryR.Text, out r))
            {
                if (r >= l && r < arraySize)
                {
                    qR = r;
                }
                else
                {
                    MessageBox.Show("Правая граница должна быть больше " + l + " и меншье " + arraySize, "Ошибка");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Некорректный ввод", "Ошибка");
                return;
            }
            calculateSteps(arraySize);
        }
    }
}
